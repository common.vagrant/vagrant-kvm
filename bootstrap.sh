yum install qemu-kvm libvirt bridge-utils virt-install -y -q
systemctl start libvirtd.service
cd /var/lib/libvirt/images/; curl -LO http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img; cd -
cd /var/lib/libvirt/images/; curl -LO https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1809.qcow2c; cd -

#cp /var/lib/libvirt/images/cirros-0.4.0-x86_64-disk.img /var/lib/libvirt/images/vm1.qcow2
#virt-install --name vm1 --ram 512 --disk path=/var/lib/libvirt/images/vm1.qcow2 --vcpus 1 --os-type linux --os-variant centos7.0 --network bridge=virbr0 --import --noautoconsole

yum install git ansible sshpass tmux -y -q
yum install epel-release -y -q
yum install python-pip ansible-lint libguestfs libguestfs-tools vim-enhanced expect -y -q
